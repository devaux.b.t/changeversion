const { createServer } = require('http');
const fs = require('fs');
const path = require('path');
const pathFS = 'package.json';
const semverRegex = require ('semver-regex');
const semverDiff = require ('semver-Diff');
const port = 3000;

const server = createServer((request, response) => {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('server on port '+port);
});


server.listen(port, '127.0.0.1', () => {
    console.log('server on port '+port);
});

function changeVersion(version){
     fs.readFile(pathFS, 'utf8', function(error, data){
        if (error){
            return console.log(error);
        } else {
            data;
        }
        const json = JSON.parse(data);
        const jsonString = JSON.stringify(data);
        console.log(jsonString);
        const semVer = semverDiff(json.version, version);
        const newPackage= "{\n  \"name\": \"changeversion\",\n  \"version\": \""+version+"\",\n  \"description\": \"\",\n  \"main\": \"Script-démarrage.js\",\n  \"scripts\": {\n  \"start\": \"node Script-démarrage.js\",\n  \"test\": \"echo \\\"Error: no test specified\\\" && exit 1\"\n},\n  \"author\": \"\",\n  \"license\": \"ISC\",\n  \"dependencies\": {\n  \"node\": \"^14.5.0\",\n  \"semver-diff\": \"^3.1.1\",\n  \"semver-regex\": \"^3.1.1\"\n }\n}"
          
        switch (semVer) {
            case 'patch':
                console.log("patch");
                fs.writeFile(pathFS, newPackage, function(error, data2){
                    if (error){
                        throw error;}
                })
                console.error("La version du package est "+semVer+" , la version "+ json.version + " est incorrect");
              break;
            case 'prepatch':
                console.log("prepatch");

                fs.writeFile(pathFS, newPackage, function(error, data2){
                  if (error){
                      throw error;}
              })
                json.version.replace(json.version,version);
                console.error("La version du package est "+semVer+" , la version "+ json.version + " est incorrect");
                break;
            case 'major':
                console.log("major");
                fs.writeFile(pathFS, newPackage, function(error, data2){
                  if (error){
                      throw error;}
              })
                json.version.replace(json.version,version);
                console.error("La version du package est "+semVer+" , la version "+ json.version + " est incorrect");
              break;
            case 'premajor':
                console.log("premajor");
                fs.writeFile(pathFS, newPackage, function(error, data2){
                  if (error){
                      throw error;}
              })
                json.version.replace(json.version,version);
                console.error("La version du package est "+semVer+" , la version "+ json.version + " est incorrect");
              break;
              case 'minor':
                console.log("minor");
                fs.writeFile(pathFS, newPackage, function(error, data2){
                  if (error){
                      throw error;}
              })
                json.version.replace(json.version,version);
                console.error("La version du package est "+semVer+" , la version "+ json.version + " est incorrect");
              break;
              case 'preminor':
                console.log("preminor");
                fs.writeFile(pathFS, newPackage, function(error, data2){
                  if (error){
                      throw error;}
              })
                json.version.replace(json.version,version);
                console.error("La version du package est "+semVer+" , la version "+ json.version + " est incorrect");
              break;
            default:
                console.log("La version du package est "+semVer+" , la version "+ json.version + " est correct");
          }
     });
}

changeVersion("1.0.2");
